<?php
declare(strict_types=1);

namespace App\Geometry;

require_once ('Square.php');

class Rectangle extends Square
{
    protected $b;

    public function __construct(float $a, float $b)
    {
        $this->b = $b;
        parent::__construct($a);
    }

    public function setB(float $b) : Square
    {
        $this->b = $b;
        return $this;
    }

    public function getB() : float
    {
        return $this->b;
    }

    public function getPerimeter() : float
    {
        return 2 * $this->a + 2 * $this->b;
    }

    public function getSquare() : float
    {
        return $this->a * $this->b;
    }
}