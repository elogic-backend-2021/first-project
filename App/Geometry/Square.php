<?php
declare(strict_types=1);

namespace App\Geometry;

class Square
{
    protected $a;

    public function __construct(float $a)
    {
        $this->a = $a;
    }

    public function setA(float $a) : Square
    {
        $this->a = $a;
        return $this;
    }

    public function getA() : float
    {
        return $this->a;
    }

    public function getPerimeter() : float
    {
        return 4 * $this->a;
    }

    public function getSquare() : float
    {
        return $this->a * $this->a;
    }
}